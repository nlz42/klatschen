#!/usr/bin/env python

import signal
from mfrc522 import SimpleMFRC522

from klatschen import Klatschen
from sharade import Sharade

game = Klatschen();
reader = SimpleMFRC522()


def newGameKlatschen():
    global game
    game = Klatschen()


def newGameSharade():
    global game
    game = Sharade()


def handler(signum, frame):
    print("Could not read NFC Tag")
    raise Exception("end of time")


def read():
    signal.signal(signal.SIGALRM, handler);
    signal.alarm(1);
    try:
        id, text = reader.read();
        signal.alarm(0);
        task = (game.getARandomTask(int(text)))
        return task;
    except Exception:
        return "Could not read NFC Tag";
