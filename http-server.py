import flask
from flask import render_template
from flask import request

app = flask.Flask(__name__)

@app.route('/', methods=['GET'])
def home():
	return render_template("index.html")

app.run(host= '0.0.0.0', port=8080)

