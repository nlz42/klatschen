
from __future__ import division
import time

import Adafruit_PCA9685

pwm = Adafruit_PCA9685.PCA9685()

# Configure min and max servo pulse lengths
servo_min = 240  # Min pulse length out of 4096 gerundet
servo_max = 495  # Max pulse length out of 4096 gerundet

# Set frequency to 60hz, good for servos.
pwm.set_pwm_freq(60)

#Laut Datenblatt:
#Pulsweite 1.5ms STOP, 2MS Full speed forward, 1MS full speed backward


def forward():
    print('Moving Forward')
    pwm.set_pwm(0, 0, 600)
    pwm.set_pwm(1, 0, 150)

def backward():
    print('Moving Backward')
    pwm.set_pwm(0, 0, servo_min)
    pwm.set_pwm(1, 0, servo_max)

def clockwise():
    print ('Moving Clockwise')
    pwm.set_pwm(0, 0, servo_max-100)
    pwm.set_pwm(1, 0, servo_max+100)

def anticlockwise():
    print ('Moving Anticlockwise')
    pwm.set_pwm(0, 0, servo_min-100)
    pwm.set_pwm(1, 0, servo_min+100)

def stop():
    pwm.set_pwm(0,0, 0) #0,0 diff = 0 damit einfach STOP, in diesem fall okay, da der Servo nicht auf 90° gestellt werden muss.
    pwm.set_pwm(1, 0, 0)
