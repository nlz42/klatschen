# Wie man das PWM Signal Berechnet:

- Frequenz 50
- Bitauflösung 4096
- Servo: min: 1ms, max 2ms

Formel: FREQUENZ*(PULSE_IN_MS/1000)*BITAUFLOESUNG

- 1000 damit die MS zu Sekunden werden, da Frequenz pro Sekunde immer ist.

## Beispiel:

Ich möchte einen Puls von 1MS erzeugen, bei eienr Frequenz von 60 HZ, Bitauflösung 4096:

60 * (1MS/1000) * 4096 = 245,76

Für Serveo: FS90R Bei 60HZ
Min: 1MS MAX 2 MS => Mitte(STOP) = 1,5MS
- Laut Formel
 - MAX 2 MS: 491,52
 - STOPP 1,5 MS: 368,64
 - MIN 1 MS: 245,76