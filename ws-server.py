from tornado import websocket
import tornado.ioloop
import motor
import gameMaster

ws_clients_dictionary = {};
ws_clients = []

class EchoWebSocket(websocket.WebSocketHandler):

    def check_origin(self, origin):
        return True

    def open(self):
        if self not in ws_clients:
            ws_clients.append(self)
            ws_clients_dictionary[0] = self
            print(self)
        print ("Websocket Opened")

    def on_message(self, message):
        print(message);
        if(message.startswith('forward')):
            motor.forward();
        if(message.startswith('backward')):
            motor.backward();
        if(message.startswith('clockwise')):
            motor.clockwise();
        if(message.startswith('anticlockwise')):
            motor.anticlockwise();
        if(message.startswith('stop')):
            motor.stop();
        if(message.startswith('read')):
            ws_clients_dictionary[0].write_message(gameMaster.read());
        if(message.startswith('newGameKlatschen')):
            gameMaster.newGameKlatschen()
            ws_clients_dictionary[0].write_message("new game started");
        if(message.startswith('newGameSharade')):
            gameMaster.newGameSharade()
            ws_clients_dictionary[0].write_message("new game started");


    def on_close(self):
        if self in ws_clients:
            ws_clients.remove(self)
        print ("Websocket closed")

application = tornado.web.Application([(r"/", EchoWebSocket),])

if __name__ == "__main__":
    application.listen(9000)
    tornado.ioloop.IOLoop.instance().start()
